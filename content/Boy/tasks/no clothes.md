---
title: "Clothes = Bad"
date: 2020-11-05T07:50:00+02:00
draft: false
---

Put ALL of your clothes in a closet (lock it if you want), then you can do whatever you usually do, but you have to record (timelapse is good enough, just need to be able to check that you aren't wearing any clothes)

Do that for at least 2 hours if your parents are home, or 6+ hours if your parents aren't home