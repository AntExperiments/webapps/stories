---
title: "Fantasy"
date: 2020-11-05T07:50:00+02:00
draft: false
description: "post a sexual fantasy of yours on reddit"
---

1. Write a fantasy of yours (the kinkier than better), at least 600 words long but preferably more
2. Send me that story, I will check it and might ask you to change it
3. It will be uploaded on [stories.any.lgbt](https://stories.any.lgbt/)
4. Post it on reddit, also make sure to link to the [stories.any.lgbt](https://stories.any.lgbt/) page

at the end, I will use a random generator (every upvote on that post will make a positive outcome 1% more likely (up to 100%)). If that random generator results a positive result, you will get a reward