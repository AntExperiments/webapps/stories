---
title: "Boy rules"
date: 2020-11-05T07:50:00+02:00
draft: false
---

## Cage and general rules

The Boy is required to wear the cage at all times. Exceptions for this include, but are not limited to: Medical reasons (like a doctors appointement) and sport.

The master is allowed to ask the Boy for a picture as proof of being locked, after sending the message to the Boy, an answer needs to be sent in max. 60 minutes time, otherwise this will result in a punishment (exceptions will be made for times where boy is unable to respond, like sleep).

## Daily routine

There are three times the boy needs to send pictures (excluding tasks) without being asked to:
1. In the morning after waking up (around 9:00)
2. After or before lunch (around 12:00)
3. After dinner (around 19:00)

Every evening (before midnight) the Boy needs to fill out the form, which will ask about various things (like amount of tasks per day, horniness, time spent naked, slept naked, and much more). Forgetting about this, will result in punishments.

## Tasks

Every day (usually in the morning), new tasks will be put live on [projects.ant.lgbt/boy](https://projects.ant.lgbt/boy/). The boy is expected to pick at least two of theses tasks and complete them on that day (unless specified otherwise).

Certain Tasks have certain rewards and punishments attached to them, some might not. If a task does not have any attached, that does not mean, that the task does not have any concequences, it just means that the boy is not aware of what these might be.

The entirety of the task must always be recorded, otherwise it will count as incomplete or invalid, and will result in punishment.

## Playing with yourself

The Boy is allowed to play with themself every day, as much as they like. If the Boy decides to publish these sessions (either by sending them to the master or other people, or publishing them on Reddit or Kik or similar), then the comments (or upvotes) on these posts might affect how long the Boy will be locked (the more positive feedback, the more likely to get rewards).

## Vetos

The Boy has the permission to stop whatever task or similar is ordered by the master by using the word: "Red", out of context. If something goes too far and breaks the limits of the Boy, they are allowed to request a stop using the Veto aswell.