---
title: "The beach - Part 2"
date: 2020-11-02T11:45:00.001Z
draft: false
categories:
- Story
- Vanilla
tags:
- beach
- roommate
- summer
- swimming
---

> DISCLAIMER: This is the first time I am writing a story like this and English isn't my native language, so expect errors and grammatical issues.

"Phil!!!", I heard as I nearly had a heart attack. Ike was coming running towards me with his arms open and I got a big surprise hug. "Hey Ike", I said trying not to be awkward. "Awwwwww, you're blushing". "No, I am not", I tried to reason, unsuccessfully, it seems. "Haha, sure. Anyways, let's go, the others are already waiting for us". As soon as those words left his mouth, he grabbed my hand and raced towards the ocean. I can't deny the fact, that I continued blushing the entire time.

He led me to a small place hidden behind some palm trees, it was really quite beautiful. His three friends where already there. "Hey guys, this is Phil", he introduced me. All of them turned to look at me. "Hey Phil", came from them perfectly simultaneously.

I tried to greet them, but my mouth would not cooperate. All of the three where completely shirtless, which made my eyes completely focused. Not making the situation any better, was that Ike was now taking his shirt off as well, right when standing next to me. "Do you just want to stay there?", said Ike. I could swear that he had a smirk on his face. Right after he said that, he went and ran straight to the ocean, his friends followed him.

I was still standing there, perplexed, but quickly regained control of my body, hastefully took off my shirt, and started to run towards the ocean as well.

---

We where swimming around for quite a bit. There was a jumping tower around 40 meters from the coast-line and we all decided to swim there and have a couple jumps. I have to say, that it was quite the surprise to see how well the others could jump, even had a couple of tricks.

When it got time for me to jump, I tried to make a salto in mid air, but I kinda failed completely. It must have looked hilarious, because Ike started laughing and soon after, the others joined him. Seeing that many half-naked guys laugh made me blush so much, but also gave me motivation to try to jump again, but this time a lot better. So I very quickly found myself again in position to jump for the second time.

Just as I was about to jump, already swinging my arms to get enough force, If felt a push from behind me and had an uncontrolled fall into the water. "Ups! Sorry", I heard coming from the now completely laughing Ike that for some reason I still counted as my best friend. Although my tries to try to look angry failed quite quickly. There is just nothing that could make me be angry at such a cute boy for more than about 10 seconds.

I was about half the way to the latter again, when I notices something, or rather noticed something missing. That jump must have somehow managed to remove my swimming-gear from me, probably loosened by the last couple jumps. And yes, that means that I am now swimming completely naked in the water, with my bestfriend laughing his adorable little ass off and his friends next to him.... Great

To be continued ...